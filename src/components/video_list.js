import React, {Component} from 'react';

import VideoListItem from './video_list_item';

const VideoList = (props) => {

  // Get video array from api and send it to list item
  const videoItems = props.video.map((videos) => {
    return <VideoListItem key={videos.etag} videos={videos} />
  });

  return(
    <ul className="col-md-4 list-group">
      {videoItems}
    </ul>
  );
}

export default VideoList;
