import React, { Component } from 'react';

// React Classes
class SearchBar extends Component {
  constructor(props) {
    super(props);

    this.state = { term: '' };
  }
  render() {
    return (
      <div>
        <input
          value={this.state.term}
          onChange={event => this.setState({ term: event.target.value })} 
        />
      </div>
    );
  }

  // This will handle change of input



}

export default SearchBar;
