import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import YTSearch from 'youtube-api-search';

// Search Bar Component
import SearchBar from './components/search_bar';
import VideoList from './components/video_list';

// Youtube api key
const API_KEY = "AIzaSyAcq8xnNAkqZ1Sye398AE5Ccr3EP8GTjaA";

// Create new component and it will produce
// some html
class App extends Component {
  constructor(props) {
      super(props);

      this.state = { videos: [] };

      // Getting data from youtube
      YTSearch({key: API_KEY, term: 'reactjs'}, (videos) => {
        this.setState({ videos:  videos});
      });
  }

  render() {
    return (
      <div>
        <SearchBar />
        <VideoList video={this.state.videos} />
      </div>
    );
  }
}


// Take this component's generated HTML and put it
// on the page (in the DOM)
ReactDOM.render(<App />, document.querySelector(".container"));
