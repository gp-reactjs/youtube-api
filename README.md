# ReduxSimpleStarter

Interested in learning [Redux](https://www.gpcoders.com/)?

###Getting Started###

There are two methods for getting started with this repo.

####Familiar with Git?#####
Checkout this repo, install dependencies, then start the gulp process with the following:

```
	> git clone https://gitlab.com/gp-reactjs/youtube-api.git
	> cd Project Folder Name
	> npm install
	> npm start
```

####Not Familiar with Git?#####
Click [here](https://gitlab.com/gp-reactjs/youtube-api/tree/master) then download the .zip file.  Extract the contents of the zip file, then open your terminal, change to the project directory, and:

```
	> npm install
	> npm start
```
